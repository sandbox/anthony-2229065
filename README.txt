Column Menu Items Module
------------------------

Overview
--------
Column menu items is a Drupal module that allows editors to create columns of
menu items. Special menu items provides similar functionality by providing
placeholder ("<nolink>").
The main difference is that Column menu items doesn't display that specific menu
item, and removes it from the breadcrumb.

Use Case
--------
This module is especially helpful for multi-columns footer menus.

Features
--------
  - User can create a new menu item and specify "<column>" in the Path field,
  without quotes.
  - The value specified in the Menu link title field is only used for the admin
  interface, and will NOT be rendered.
  - Breadcrumbs will NOT display that menu item neither.
  - CSS class "column" will be added on the "<li>" element containing the
  sub-menu.
  - The "column" CSS class is configurable.

Installation
------------
  1. Copy the column_menu_items folder to your sites/all/modules directory.
  2. At Administer -> Site building -> Modules (admin/modules) enable the
  module.
  3. Configure the module settings at Administer -> Site configuration -> Column
  Menu Items (admin/config/system/column_menu_items).

Compatibilities with other modules
----------------------------------
Column menu items should play nicely with
Menu Block (http://drupal.org/project/menu_block) or
Special Menu Items (http://drupal.org/project/special_menu_items).

Credits
-------
Anthony Riberi (Stanto) - http://drupal.org/user/653902
This module comes from a patch (https://drupal.org/node/1475370) for Special
Menu Items created by mpotter (https://drupal.org/user/616192) and updated by
tirdadc (https://drupal.org/user/383630).
