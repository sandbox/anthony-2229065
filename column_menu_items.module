<?php

/**
 * @file
 * Module to create groups of menu items.
 */

/**
 * Implements hook_menu().
 */
function column_menu_items_menu() {
  $items['<column>'] = array(
    'page callback' => 'drupal_not_found',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/system/column_menu_items'] = array(
    'title' => 'Column Menu Items',
    'description' => 'Configure Column Menu Items.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('column_menu_items_admin_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Override of theme_link().
 */
function column_menu_items_link(array $variables) {
  if (in_array($variables['path'], array('<column>'))) {
    // Removing the menu item if it is a column.
    return FALSE;
  }
  // Call the original theme function for normal menu link.
  return theme('column_menu_items_link_default', $variables);
}


/**
 * Implements hook_theme_registry_alter().
 *
 * We replace theme_menu_item_link with our own function.
 */
function column_menu_items_theme_registry_alter(&$registry) {
  // Save previous value from registry in case another theme overwrites
  // menu_item_link
  $registry['column_menu_items_link_default'] = $registry['link'];
  $registry['link']['function'] = 'column_menu_items_link';
}

/**
 * Implements hook_form_FROM_ID_alter().
 *
 * Description changed, added column as path types.
 */
function column_menu_items_form_menu_edit_item_alter(&$form, &$form_state) {
  if (isset($form['link_path']['#default_value'])) {
    $default_value = $form['link_path']['#default_value'];

    if (preg_match('/^<column>\/[0-9]+$/', $default_value)) {
      $default_value = '<column>';
    }

    $form['link_path']['#default_value'] = $default_value;
    $form['link_path']['#description'] .= ' ' . t('Enter "%column" to generate a column.', array('%column' => '<column>'));
  }
  if (isset($form['expanded'])) {
    $form['expanded']['#description'] .= ' ' . t('If the link path is set as "%column", this checkbox will be enabled.', array('%column' => '<column>'));
  }
  array_unshift($form['#submit'], 'column_menu_items_form_menu_edit_item_submit');
}

/**
 * Custom submit handler for column_menu_items_form_menu_edit_item_alter.
 */
function column_menu_items_form_menu_edit_item_submit($form, &$form_state) {
  if ($form_state['values']['link_path'] == '<column>') {
    $form_state['values']['expanded'] = 1;
  }
}

/**
 * Implements hook_init().
 */
function column_menu_items_init() {
  // Removing column breadcrumb item.
  $breadcrumb = drupal_get_breadcrumb();
  foreach ($breadcrumb as $key => $crumb) {
    if ($crumb === FALSE) {
      unset($breadcrumb[$key]);
    }
  }
  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Column Menu Items admin settings form.
 */
function column_menu_items_admin_settings_form() {
  $form['column_menu_items_class'] = array(
    '#type' => 'textfield',
    '#title' => t('HTML class for "column"'),
    '#description' => t('By default, it will use the class "column" for the column. Here you can specify your own class.'),
    '#default_value' => variable_get('column_menu_items_class', 'column'),
  );

  return system_settings_form($form);
}

/**
 * Implements MODULE_preprocess_HOOK().
 *
 * Adds appropriate attributes to the list item.
 *
 * @see theme_menu_link()
 */
function column_menu_items_preprocess_menu_link(&$variables) {
  $href = $variables['element']['#href'];
  $attributes = &$variables['element']['#attributes'];

  if ($href == '<column>') {
    $attributes['class'][] = variable_get('column_menu_items_class', 'column');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Appends the attached block's title to the title of the menu item.
 */
function column_menu_items_form_menu_overview_form_alter(&$form, &$form_state) {
  $elements = element_children($form);
  foreach ($elements as $mlid) {
    $element = &$form[$mlid];
    // Only process menu items.
    if (isset($element['#item'])) {
      $item = &$element['#item'];
      if ($item['link_path'] == '<column>') {
        $element['title']['#markup'] = '<div class="column-menu-item">Column:  ' . check_plain($item['title']) . ' </div>';
      }
    }
  }
}
